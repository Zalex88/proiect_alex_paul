package wantsome.project.DB.dto;

import java.util.Objects;

public class ProprietarDTO {

    private final int id;
    private final String nume;
    private final String prenume;
    private final String adresa;
    private final String oras;
    private final String telefon;
    private final String email;

    public ProprietarDTO(int id, String nume, String prenume, String adresa,
                         String oras, String telefon, String email) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.adresa = adresa;
        this.oras = oras;
        this.telefon = telefon;
        this.email = email;
    }

    //version without id, needed for the case of new proprietar to insert in db (when id is not yet set/known)
    public ProprietarDTO(String nume, String prenume, String adresa,
                         String oras, String telefon, String email) {
        this(-1, nume, prenume, adresa, oras, telefon, email);
    }

    public int getId() {
        return id;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getOras() {
        return oras;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getEmail() {
        return email;
    }


    @Override
    public String toString() {
        return "ProprietarDTO{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", prenume='" + prenume + '\'' +
                ", adresa='" + adresa + '\'' +
                ", oras='" + oras + '\'' +
                ", telefon='" + telefon + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProprietarDTO that = (ProprietarDTO) o;
        return id == that.id &&
                Objects.equals(nume, that.nume) &&
                Objects.equals(prenume, that.prenume) &&
                Objects.equals(adresa, that.adresa) &&
                Objects.equals(oras, that.oras) &&
                Objects.equals(telefon, that.telefon) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nume, prenume, adresa, oras, telefon, email);
    }
}
