package wantsome.project.DB.dto;

import java.util.Objects;

public class RasaDTO {

    private final int id;
    private final String rasa;
    private final int specieId;

    public RasaDTO(int id, String rasa, int specieId) {
        this.id = id;
        this.rasa = rasa;
        this.specieId = specieId;
    }

    public RasaDTO(String rasa, int specieId) {
        this(-1, rasa, specieId);
    }

    public int getId() {
        return id;
    }

    public String getRasa() {
        return rasa;
    }

    public int getSpecieId() {
        return specieId;
    }

    @Override
    public String toString() {
        return "RasaDTO{" +
                "id=" + id +
                ", rasa='" + rasa + '\'' +
                ", specie_id='" + specieId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RasaDTO rasaDTO = (RasaDTO) o;
        return id == rasaDTO.id &&
                Objects.equals(rasa, rasaDTO.rasa) &&
                Objects.equals(specieId, rasaDTO.specieId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rasa, specieId);
    }
}
