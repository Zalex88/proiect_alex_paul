package wantsome.project;

import wantsome.project.service.InserareDate;
import wantsome.project.service.PetDdl;
import wantsome.project.service.PetsDao;
import wantsome.project.web.*;

import java.sql.SQLException;

import static spark.Spark.*;

/**
 * Main class to start the web application
 */
public class Main {

    public static void main(String[] args) throws SQLException {
        createTablesAndSampleData();
        configureRoutes();

        awaitInitialization();
        System.out.println("Server started: http://localhost:4567/main");
    }

    private static void createTablesAndSampleData() throws SQLException {
        PetDdl.createTableSpecie();
        PetDdl.createTableRase();
        PetDdl.createTableProprietari();
        PetDdl.createTablePets();
        PetDdl.createTableExamen();
        if (PetsDao.getAll().isEmpty()) {
            InserareDate.insertIntoPets();
        }
    }

    private static void configureRoutes() {
        staticFileLocation("/public"); //location of static resources (like images, .css ..), relative to /resources dir

        get("/main", MainPageController::showPage);
        get("/latestPatients", MainPageController::latestTenAdded);
        get("/allPacients", MainPageController::allAdded);
        get("/delete/:id", MainPageController::deletePet);
        post("/main", MainPageController::searchBy);

        get("/add", AddPetController::showPageAdd);
        post("/add", AddPetController::addFisaPacient);

        get("/pet/:id", Addpet::showAddForm);
        post("/pet/:id", Addpet::addPet);

        get("/addExamen/:id", AddExamen::showAddUpdateForm);
        post("/addExamen/:id", AddExamen::addExamen);

        get("/loadFisa/:id", UpdatePageControler::showAddUpdateForm);
        post("/loadFisa/:id", UpdatePageControler::doUpdate);
    }
}