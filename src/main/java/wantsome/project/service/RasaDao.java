package wantsome.project.service;

import wantsome.project.DB.dto.RasaDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RasaDao {

    public static List<RasaDTO> getAll() {
        List<RasaDTO> rase = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("select * from rasa order by id")) {

            while (rs.next()) {

                int id = rs.getInt("id");
                String descriere = rs.getString("descriere");
                int specieId = rs.getInt("specie_id");
                RasaDTO rasa = new RasaDTO(id, descriere, specieId);

                rase.add(rasa);
            }
        } catch (SQLException e) {
            System.out.println("Error loading rase " + e.getMessage());
        }
        return rase;
    }


    public static void insert(String descriere, int id) {
        String sql = "INSERT INTO rasa" +
                "(" + "descriere" + "," + "specie_id" + ")" +
                "VALUES(?, ?)";//2 values

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, descriere);
            ps.setInt(2, id);

            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error inserting in db pets " + descriere + " : " + e.getMessage());
        }
    }


    static RasaDTO extractNoteFromResult(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String descriere = rs.getString("descriere");
        int specieId = rs.getInt("specie_id");
        RasaDTO rasa = new RasaDTO(id, descriere, specieId);
        System.out.println(rasa);
        return rasa;
    }


    public static void update(RasaDTO rasa) {
        String sql = "UPDATE rasa SET descriere =? WHERE id =?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, rasa.getRasa());
            ps.setInt(2, rasa.getId());
            //ps.setInt(3, rasa.getSpecie_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating rasa " + rasa + " : " + e.getMessage());
        }
    }


    public void delete(long id) {

        String sql = "DELETE FROM " + "rasa" + " WHERE " + "id" + " = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting rasa " + id + ": " + e.getMessage());
        }
    }


    public static int findId(String word) {
        int code = 0;
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT id, descriere from rasa \n" +
                     "where descriere like \"" + word + "\"")) {

            while (rs.next()) {
                code = rs.getInt("id");
            }

        } catch (SQLException e) {
            System.err.println("Error inserting in db pets " + "rasa" + " : " + e.getMessage());
        }

        return code;
    }

    public static Optional<RasaDTO> findRasa(int id) {
        String sql = "SELECT * from rasa\n" +
                "where id = ?;";
        {

            try (Connection conn = DbManager.getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql)) {
                //maybe not necessary
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        RasaDTO rasa = RasaDao.extractNoteFromResult(rs);
                        System.out.println(Optional.of(rasa));
                        return Optional.of(rasa);
                    }
                }
            } catch (SQLException e) {
                System.err.println("Error loading Rasa document with id " + id + " : " + e.getMessage());
            }
            return Optional.empty();
        }
    }

}
