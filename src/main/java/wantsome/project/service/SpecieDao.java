package wantsome.project.service;

import wantsome.project.DB.dto.SpecieDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SpecieDao {


    public static List<SpecieDTO> getAll() {
        List<SpecieDTO> species = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("select * from specie order by id")) {

            while (rs.next()) {

                int id = rs.getInt("id");
                String descriere = rs.getString("descriere");

                SpecieDTO specie = new SpecieDTO(id, descriere);
                species.add(specie);
            }
        } catch (SQLException e) {
            System.out.println("Error loading rase " + e.getMessage());
        }
        return species;
    }


    public static int findId(String specie) {
        int id = 0;

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT id from specie where descriere like \"" + specie + "\"")) {

            id = rs.getInt("id");


        } catch (SQLException e) {
            System.err.println("Error could not find " + specie + " : " + e.getMessage());
        }
        return id;
    }

    public static Optional<SpecieDTO> findSpecie(int id) {
        String sql = "SELECT * from specie\n" +
                "where id = ?;";
        {

            try (Connection conn = DbManager.getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql)) {
                //maybe not necessary
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        SpecieDTO specie = SpecieDao.extractNoteFromResult(rs);
                        return Optional.of(specie);
                    }
                }
            } catch (SQLException e) {
                System.err.println("Error loading Specie document with id " + id + " : " + e.getMessage());
            }
            return Optional.empty();
        }
    }


    public static void insert(String specie) {
        String sql = "INSERT INTO specie" +
                "(" + "descriere" + ")" +
                "VALUES(?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, specie);
            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error inserting in db pets " + specie + " : " + e.getMessage());
        }
    }


    private static SpecieDTO extractNoteFromResult(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String descriere = rs.getString("descriere");
        return new SpecieDTO(id, descriere);
    }


    public static void update(SpecieDTO specie) {
        String sql = "UPDATE specie SET descriere =? WHERE id =?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, specie.getSpecie());
            ps.setLong(2, specie.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating specie " + specie + " : " + e.getMessage());
        }
    }


    public void delete(long id) {

        String sql = "DELETE FROM " + "specie" + " WHERE " + "id" + " = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting specie " + id + ": " + e.getMessage());
        }
    }

}
