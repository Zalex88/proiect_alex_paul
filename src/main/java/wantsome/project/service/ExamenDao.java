package wantsome.project.service;

import wantsome.project.DB.dto.ExamenDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExamenDao {

    public static List<ExamenDTO> getAll(int idPet) throws SQLException {

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("select * from examen where PET_ID = " + idPet + " order by id")) {

            List<ExamenDTO> examene = new ArrayList<>();
            while (rs.next()) {

                int id = rs.getInt("id");
                int petId = rs.getInt("pet_id");
                Date data = rs.getDate("data_examen");
                String anamneza = rs.getString("anamneza");
                String examenClinic = rs.getString("examen_clinic");
                String diagnostic = rs.getString("diagnostic");
                String tratament = rs.getString("tratament");

                ExamenDTO examen = new ExamenDTO(id, petId, data, anamneza, examenClinic, diagnostic, tratament);
                examene.add(examen);
            }
            return examene;
        }
    }


    public static void insert(ExamenDTO examen) {
        String sql = "INSERT INTO examen (" +
                "pet_id , data_examen , anamneza , examen_clinic , diagnostic , tratament)" +
                "VALUES(?, ?, ?, ?, ?, ?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, examen.getPetId());
            ps.setDate(2, (java.sql.Date) examen.getDate());
            ps.setString(3, examen.getAnamneza());
            ps.setString(4, examen.getExamenClinica());
            ps.setString(5, examen.getDiagnostic());
            ps.setString(6, examen.getTratament());

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error inserting  Examen in db  " + examen + " : " + e.getMessage());
        }
    }


    public static void update(ExamenDTO examen) {
        String sql = "UPDATE examen SET pet_id  =?," +
                "data_examen =?," +
                "anamneza =?," +
                "examen_clinic =?," +
                "diagnostic =?," +
                "tratament =?" +
                "WHERE id =?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, examen.getPetId());
            ps.setDate(2, (java.sql.Date) examen.getDate());
            ps.setString(3, examen.getAnamneza());
            ps.setString(4, examen.getExamenClinica());
            ps.setString(5, examen.getDiagnostic());
            ps.setString(6, examen.getTratament());
            ps.setLong(7, examen.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating examen " + examen + " : " + e.getMessage());
        }
    }

    public static void delete(long id) {

        String sql = "DELETE FROM examen WHERE PET_ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting examen " + id + ": " + e.getMessage());
        }
    }

}
