package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.DB.dto.ExamenDTO;
import wantsome.project.DB.dto.PetsDTO;
import wantsome.project.DB.dto.ProprietarDTO;
import wantsome.project.service.*;

import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class AddPetController {

    private static int proprietarId = 0;

    public static String showPageAdd(Request req, Response res) {
        Map<String, Object> model = new HashMap<>();
        model.put("rase", RasaDao.getAll());
        model.put("specii", SpecieDao.getAll());
        return render(model, "add.vm");
    }

    public static Object addFisaPacient(Request req, Response res) throws SQLException {
        handleAddUpdateRequestProprietar(req, res);
        handleAddSpecie(req, res);
        handleAddRasa(req, res);
        handleAddUpdateRequestPet(req, res);
        handleAddUpdateRequestExamen(req, res);
        return MainPageController.latestTenAdded(req, res);
    }

    private static void handleAddSpecie(Request req, Response res) {
        String specie = req.queryParams("specie");
        try {
            SpecieDao.insert(specie);
        } catch (Exception e) {
            System.out.println("Error inserting '" + specie + "': " + e.getMessage());
        }
    }

    private static void handleAddRasa(Request req, Response res) {
        String rasa = req.queryParams("rasa");
        String specie = req.queryParams("specie");
        try {
            int id = SpecieDao.findId(specie);
            RasaDao.insert(rasa, id);
        } catch (Exception e) {
            System.out.println("Error inserting '" + rasa + "': " + e.getMessage());
        }
    }

    private static void handleAddUpdateRequestPet(Request req, Response res) {
        String descriere = req.queryParams("rasa");
        String numeAnimal = req.queryParams("numeAnimal");
        String culoare = req.queryParams("culoare");
        String sex = req.queryParams("sex");
        String greutate = req.queryParams("greutate");
        String varsta = req.queryParams("varsta");
        String microcip = req.queryParams("microcip");
        String nume = req.queryParams("nume");
        String prenume = req.queryParams("prenume");
        String email = req.queryParams("email");

        int rasaId = RasaDao.findId(descriere);
        proprietarId = ProprietariDao.findId(nume, prenume, email);
        System.out.println(proprietarId + descriere + numeAnimal + culoare + sex + greutate + varsta + microcip + nume + prenume + email);

        try {
            PetsDTO animal = validateAndBuildPet(rasaId, numeAnimal, microcip, culoare, proprietarId, sex, greutate, varsta);
            PetsDao.insert(animal);
        } catch (Exception e) {
            System.out.println("Error in the pet department");
        }
    }


    private static PetsDTO validateAndBuildPet(int rasaId, String numeAnimal, String microcip, String culoare,
                                               int propId, String sex, String greutate, String varsta) {

        if (numeAnimal == null || numeAnimal.isEmpty()) {
            throw new RuntimeException("Nume is required!");
        }

        if (culoare == null || culoare.isEmpty()) {
            throw new RuntimeException("Culoare is required!");
        }

        if (sex == null || sex.isEmpty()) {
            throw new RuntimeException("Sex is required!");
        }

        if (greutate == null || greutate.isEmpty()) {
            throw new RuntimeException("Greutate is required!");
        }

        if (varsta == null || varsta.isEmpty()) {
            throw new RuntimeException("Varsta is required!");
        }

        return new PetsDTO(numeAnimal, rasaId, Integer.parseInt(microcip), culoare, propId, sex, Double.parseDouble(greutate), Double.parseDouble(varsta));
    }

    private static void handleAddUpdateRequestProprietar(Request req, Response res) {
        String nume = req.queryParams("nume");
        String prenume = req.queryParams("prenume");
        String adresa = req.queryParams("adresa");
        String oras = req.queryParams("oras");
        String telefon = req.queryParams("telefon");
        String email = req.queryParams("email");

        try {
            ProprietarDTO proprietar = validateAndBuildProprietar(nume, prenume, adresa, oras, telefon, email);
            ProprietariDao.insert(proprietar);
        } catch (Exception e) {
            System.out.println("Error in the proprietar db");
        }
    }

    private static ProprietarDTO validateAndBuildProprietar(String nume, String prenume, String adresa, String oras, String telefon, String email) {
        if (nume == null || nume.isEmpty()) {
            throw new RuntimeException("Nume is required!");
        }

        if (prenume == null || prenume.isEmpty()) {
            throw new RuntimeException("Prenume is required!");
        }

        if (adresa == null || adresa.isEmpty()) {
            throw new RuntimeException("Adresa is required!");
        }

        if (oras == null || oras.isEmpty()) {
            throw new RuntimeException("Oras is required!");
        }

        if (telefon == null || telefon.isEmpty()) {
            throw new RuntimeException("Telefon is required!");
        }

        if (email == null || email.isEmpty()) {
            throw new RuntimeException("Email is required!");
        }

        return new ProprietarDTO(nume, prenume, adresa, oras, telefon, email);
    }

    private static void handleAddUpdateRequestExamen(Request req, Response res) {
        String data = req.queryParams("data");
        String anamneza = req.queryParams("anamneza");
        String examenClinic = req.queryParams("examen_clinic");
        String diagnostic = req.queryParams("diagnostic");
        String tratament = req.queryParams("tratament");
        String numeAnimal = req.queryParams("numeAnimal");

        int petId = PetsDao.findId(numeAnimal, proprietarId);
        System.out.println(numeAnimal);
        System.out.println(proprietarId);
        System.out.println(petId);

        try {
            ExamenDTO examen = validateAndBuildExamen(petId, data, anamneza, examenClinic, diagnostic, tratament);
            ExamenDao.insert(examen);
        } catch (Exception e) {
            System.out.println("Error in the examen db");
        }
    }

    private static ExamenDTO validateAndBuildExamen(int petId, String data, String anamneza, String examenClinic, String diagnostic, String tratametn) {
        Date selectedDate = null;
        if (data != null && !data.isEmpty()) {
            try {
                selectedDate = Date.valueOf(data);
            } catch (Exception e) {
                throw new RuntimeException("Invalid due date value: '" + data +
                        "', must be a date in format: dd-mm-yyyy");
            }
        }

        if (anamneza == null || anamneza.isEmpty()) {
            throw new RuntimeException("Anamneza is required!");
        }

        if (examenClinic == null || examenClinic.isEmpty()) {
            throw new RuntimeException("Examen Clinic is required!");
        }

        if (diagnostic == null || diagnostic.isEmpty()) {
            throw new RuntimeException("Diagnostic is required!");
        }

        if (tratametn == null || tratametn.isEmpty()) {
            throw new RuntimeException("Tratament is required!");
        }

        return new ExamenDTO(petId, selectedDate, anamneza, examenClinic, diagnostic, tratametn);
    }
}
