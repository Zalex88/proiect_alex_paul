package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.DB.dto.PetsDTO;
import wantsome.project.DB.dto.ProprietarDTO;
import wantsome.project.DB.dto.RasaDTO;
import wantsome.project.service.DbManager;
import wantsome.project.service.ExamenDao;
import wantsome.project.service.PetsDao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class MainPageController {

    private static List<ProprietarDTO> proprietari;
    private static List<PetsDTO> pets;
    private static List<RasaDTO> rase;
    private static String chooseButton = "Recent adaugati";


    public static String showPage(Request req, Response res) {
        Map<String, Object> model = new HashMap<>();
        model.put("butonSelect", chooseButton);
        model.put("proprietari", proprietari);
        model.put("animalut", pets);
        model.put("rase", rase);
        return render(model, "main.vm");
    }

    public static Object latestTenAdded(Request req, Response res) throws SQLException {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT p.id , p.prenume , p.nume , p.telefon ,  p.email, pet.nume as numeAnimal, pet.id as idAnimal, r.descriere\n" +
                     "from proprietar p, pets pet, rasa r\n" +
                     "where p.id = pet.proprietar_id\n" +
                     "and pet.rasa_id = r.id\n" +
                     "order by pet.id desc\n" +
                     "limit 10;")) {

            proprietari = new ArrayList<>();
            pets = new ArrayList<>();
            rase = new ArrayList<>();
            while (rs.next()) {
                //Proprietar
                int id = rs.getInt("id");
                String nume = rs.getString("nume");
                String prenume = rs.getString("prenume");
                String adresa = " ";
                String oras = " ";
                String tel = rs.getString("telefon");
                String email = rs.getString("email");

                ProprietarDTO proprietar = new ProprietarDTO(id, nume, prenume, adresa, oras, tel, email);
                proprietari.add(proprietar);

                //Animal
                int idA = rs.getInt("idAnimal");
                int rasaId = 0;
                String numeA = rs.getString("numeAnimal");
                int microcip = 0;
                String culoare = "";
                int proprietarId = 0;
                String sex = "";
                int greutate = 0;
                int age = 0;

                PetsDTO pet = new PetsDTO(idA, numeA, rasaId, microcip, culoare, proprietarId, sex, greutate, age);

                pets.add(pet);

                //Rasa
                int idR = 0;
                String descriere = rs.getString("descriere");
                int specieId = 0;
                RasaDTO rasa = new RasaDTO(idR, descriere, specieId);
                rase.add(rasa);
                chooseButton = "Toti pacientii";
            }

        }
        res.redirect("/main");
        return res;
    }


    public static Object allAdded(Request req, Response res) throws SQLException {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT p.id , p.prenume , p.nume , p.telefon ,  p.email, pet.nume as numeAnimal, pet.id as idAnimal, r.descriere\n" +
                     "from proprietar p, pets pet, rasa r\n" +
                     "where p.id = pet.proprietar_id\n" +
                     "and pet.rasa_id = r.id\n" +
                     "order by pet.id desc;")) {

            proprietari = new ArrayList<>();
            pets = new ArrayList<>();
            rase = new ArrayList<>();
            while (rs.next()) {
                //Proprietar
                int id = rs.getInt("id");
                String nume = rs.getString("nume");
                String prenume = rs.getString("prenume");
                String adresa = " ";
                String oras = " ";
                String tel = rs.getString("telefon");
                String email = rs.getString("email");

                ProprietarDTO proprietar = new ProprietarDTO(id, nume, prenume, adresa, oras, tel, email);
                proprietari.add(proprietar);

                //Animal*
                int idA = rs.getInt("idAnimal");
                int rasaId = 0;
                String numeA = rs.getString("numeAnimal");
                int microcip = 0;
                String culoare = "";
                int proprietarId = 0;
                String sex = "";
                int greutate = 0;
                int age = 0;

                PetsDTO pet = new PetsDTO(idA, numeA, rasaId, microcip, culoare, proprietarId, sex, greutate, age);

                pets.add(pet);

                //Rasa
                int idR = 0;
                String descriere = rs.getString("descriere");
                int specieId = 0;
                RasaDTO rasa = new RasaDTO(idR, descriere, specieId);
                rase.add(rasa);
                chooseButton = "Recent adaugati";
            }

        }
        res.redirect("/main");
        return res;
    }


    public static Object searchBy(Request req, Response res) throws SQLException {
        String value = req.queryParams("search");
        String radioButton = req.queryParams("searchBy");
        try {
            if (radioButton.equals("Nume")) {
                searchByProprietar(value);
            } else if (radioButton.equals("Microcip")) {
                searchByMicrocip(Integer.parseInt(value));
            } else {
                searchByDiagnostic(value);
            }
        } catch (NullPointerException e) {
            System.out.println("No radio button selected");
        }

        res.redirect("/main");
        return res;
    }


    public static void searchByMicrocip(int number) throws SQLException {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT p.id , p.prenume , p.nume , p.telefon ,p.email, pet.nume as numeAnimal, pet.id as idAnimal, r.descriere\n" +
                     "       from proprietar p, pets pet, rasa r\n" +
                     "       where p.id = pet.proprietar_id and pet.rasa_id = r.id and pet.microcip like \"%" + number + "%\"")) {

            proprietari = new ArrayList<>();
            pets = new ArrayList<>();
            rase = new ArrayList<>();
            while (rs.next()) {
                //Proprietar
                int id = rs.getInt("id");
                String nume = rs.getString("nume");
                String prenume = rs.getString("prenume");
                String adresa = " ";
                String oras = " ";
                String tel = rs.getString("telefon");
                String email = rs.getString("email");

                ProprietarDTO proprietar = new ProprietarDTO(id, nume, prenume, adresa, oras, tel, email);
                proprietari.add(proprietar);

                //Animal
                int idA = rs.getInt("idAnimal");
                int rasaId = 0;
                String numeA = rs.getString("numeAnimal");
                int microcip = 0;
                String culoare = "";
                int proprietarId = 0;
                String sex = "";
                int greutate = 0;
                int age = 0;

                PetsDTO pet = new PetsDTO(idA, numeA, rasaId, microcip, culoare, proprietarId, sex, greutate, age);

                pets.add(pet);

                //Rasa
                int idR = 0;
                String descriere = rs.getString("descriere");
                int specieId = 0;
                RasaDTO rasa = new RasaDTO(idR, descriere, specieId);
                rase.add(rasa);
            }
        }
    }


    public static void searchByProprietar(String nume) throws SQLException {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT p.id , p.prenume , p.nume , p.telefon ,p.email, pet.nume as numeAnimal,pet.id as idAnimal, r.descriere\n" +
                     "                     from proprietar p, pets pet, rasa r \n" +
                     "                     where p.id = pet.proprietar_id and pet.rasa_id = r.id and p.nume like \"%" + nume + "%\"")) {

            proprietari = new ArrayList<>();
            pets = new ArrayList<>();
            rase = new ArrayList<>();
            while (rs.next()) {
                //Proprietar
                int id = rs.getInt("id");
                String numeP = rs.getString("nume");
                String prenume = rs.getString("prenume");
                String adresa = " ";
                String oras = " ";
                String tel = rs.getString("telefon");
                String email = rs.getString("email");

                ProprietarDTO proprietar = new ProprietarDTO(id, numeP, prenume, adresa, oras, tel, email);
                proprietari.add(proprietar);

                //Animal
                int idA = rs.getInt("idAnimal");
                int rasaId = 0;
                String numeA = rs.getString("numeAnimal");
                int microcip = 0;
                String culoare = "";
                int proprietarId = 0;
                String sex = "";
                int greutate = 0;
                int age = 0;

                PetsDTO pet = new PetsDTO(idA, numeA, rasaId, microcip, culoare, proprietarId, sex, greutate, age);

                pets.add(pet);

                //Rasa
                int idR = 0;
                String descriere = rs.getString("descriere");
                int specieId = 0;
                RasaDTO rasa = new RasaDTO(idR, descriere, specieId);
                rase.add(rasa);
            }
        }
    }


    public static void searchByDiagnostic(String diagnostic) throws SQLException {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT p.id , p.prenume , p.nume , p.telefon ,p.email, pet.nume as numeAnimal,pet.id as idAnimal, r.descriere\n" +
                     "                     from proprietar p, pets pet, rasa r, examen e\n" +
                     "                     where p.id = pet.proprietar_id and pet.rasa_id = r.id and pet.id = e.PET_ID and e.DIAGNOSTIC like \"%" + diagnostic + "%\"")) {

            proprietari = new ArrayList<>();
            pets = new ArrayList<>();
            rase = new ArrayList<>();
            while (rs.next()) {
                //Proprietar
                int id = rs.getInt("id");
                String nume = rs.getString("nume");
                String prenume = rs.getString("prenume");
                String adresa = " ";
                String oras = " ";
                String tel = rs.getString("telefon");
                String email = rs.getString("email");

                ProprietarDTO proprietar = new ProprietarDTO(id, nume, prenume, adresa, oras, tel, email);
                proprietari.add(proprietar);

                //Animal
                int idA = rs.getInt("idAnimal");
                int rasaId = 0;
                String numeA = rs.getString("numeAnimal");
                int microcip = 0;
                String culoare = "";
                int proprietarId = 0;
                String sex = "";
                int greutate = 0;
                int age = 0;

                PetsDTO pet = new PetsDTO(idA, numeA, rasaId, microcip, culoare, proprietarId, sex, greutate, age);

                pets.add(pet);

                //Rasa
                int idR = 0;
                String descriere = rs.getString("descriere");
                int specieId = 0;
                RasaDTO rasa = new RasaDTO(idR, descriere, specieId);
                rase.add(rasa);
            }
        }
    }


    public static void searchByData(java.util.Date date) throws SQLException {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT p.id , p.prenume , p.nume , p.telefon ,p.email, pet.nume as numeAnimal,pet.id as idAnimal, r.descriere\n" +
                     "from proprietar p, pets pet, rasa r, examen e\n" +
                     "where p.id = pet.proprietar_id and pet.rasa_id = r.id and pet.id = e.PET_ID and e.DATA_EXAMEN like \"%" + date + "%\"")) {

            proprietari = new ArrayList<>();
            pets = new ArrayList<>();
            rase = new ArrayList<>();
            while (rs.next()) {
                //Proprietar
                int id = rs.getInt("id");
                String nume = rs.getString("nume");
                String prenume = rs.getString("prenume");
                String adresa = " ";
                String oras = " ";
                String tel = rs.getString("telefon");
                String email = rs.getString("email");

                ProprietarDTO proprietar = new ProprietarDTO(id, nume, prenume, adresa, oras, tel, email);
                proprietari.add(proprietar);

                //Animal
                int idA = rs.getInt("idAnimal");
                int rasaId = 0;
                String numeA = rs.getString("numeAnimal");
                int microcip = 0;
                String culoare = "";
                int proprietarId = 0;
                String sex = "";
                int greutate = 0;
                int age = 0;

                PetsDTO pet = new PetsDTO(idA, numeA, rasaId, microcip, culoare, proprietarId, sex, greutate, age);

                pets.add(pet);

                //Rasa
                int idR = 0;
                String descriere = rs.getString("descriere");
                int specieId = 0;
                RasaDTO rasa = new RasaDTO(idR, descriere, specieId);
                rase.add(rasa);
                //chooseButton = "Recent adaugati";
            }
        }
    }


    public static Object deletePet(Request req, Response res) throws SQLException {
        String id = req.params("id");
        System.out.println(id);
        ExamenDao.delete(Integer.parseInt(id));
        PetsDao.delete(Integer.parseInt(id));


        return latestTenAdded(req, res);
    }

}
