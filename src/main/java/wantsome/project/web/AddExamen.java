package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.DB.dto.ExamenDTO;
import wantsome.project.DB.dto.PetsDTO;
import wantsome.project.DB.dto.RasaDTO;
import wantsome.project.DB.dto.SpecieDTO;
import wantsome.project.service.ExamenDao;
import wantsome.project.service.PetsDao;
import wantsome.project.service.RasaDao;
import wantsome.project.service.SpecieDao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddExamen {

    public static String showAddUpdateForm(Request req, Response res) {

        String id = req.params("id");
        boolean isUpdate = id != null && !id.isEmpty();

        //Java 8 - Optional Class. Advertisements. Optional is a container object used to contain not-null objects. Optional object is used to represent null with absent value. This class
        // has various utility methods to facilitate code to handle values as 'available' or 'not available' instead of checking null values.
        Optional<PetsDTO> pet = Optional.empty();
        if (isUpdate) {
            try {
                pet = PetsDao.findPet(Integer.parseInt(id));
            } catch (Exception e) {
                System.err.println("Error loading fisa addExamen with id '" + id + "': " + e.getMessage());
            }
            if (!pet.isPresent()) {
                return "Error: pet " + id + " not found!";
            }
        }

        return renderAddUpdateForm(
                pet.map(i -> String.valueOf(i.getId())).orElse(""),
                pet.map(i -> i.getNume()).orElse(""),
                pet.map(i -> String.valueOf(i.getMicrocip())).orElse(""),
                pet.map(i -> i.getCuloare()).orElse(""),
                pet.map(i -> i.getSex()).orElse(""),
                pet.map(i -> String.valueOf(i.getGreutate())).orElse(""),
                pet.map(i -> String.valueOf(i.getVarsta())).orElse(""),
                pet.map(i -> String.valueOf(i.getRasaId())).orElse(""));
    }

    private static String renderAddUpdateForm(String idAnimal, String numeAnimal, String microcip, String culoare,
                                              String sex, String greutate, String varsta, String idRasa) {
        Map<String, Object> model = new HashMap<>();
        Optional<RasaDTO> rasa = RasaDao.findRasa(Integer.parseInt(idRasa));
        Optional<SpecieDTO> specie = SpecieDao.findSpecie(rasa.map(i -> i.getSpecieId()).orElse(0));
        String newRasa = rasa.map(i -> String.valueOf(i.getRasa())).orElse("");
        String newSpecie = specie.map(i -> String.valueOf(i.getSpecie())).orElse("");
        model.put("idAnimal", idAnimal);
        model.put("numeAnimal", numeAnimal);
        model.put("culoare", culoare);
        model.put("sex", sex);
        model.put("microcip", microcip);
        model.put("greutate", greutate);
        model.put("varsta", varsta);
        model.put("idRasa", idRasa);
        model.put("rasa", newRasa);
        model.put("specie", newSpecie);
        model.put("isUpdate", idAnimal != null && !idAnimal.isEmpty());
        return render(model, "addExamen.vm");
    }


    public static Object addExamen(Request req, Response res) throws SQLException {
        handleAddUpdateRequestExamen(req, res);
        return MainPageController.latestTenAdded(req, res);
    }

    private static void handleAddUpdateRequestExamen(Request req, Response res) {
        String data = req.queryParams("data");
        String anamneza = req.queryParams("anamneza");
        String examenClinic = req.queryParams("examen_clinic");
        String diagnostic = req.queryParams("diagnostic");
        String tratament = req.queryParams("tratament");
        String numeAnimal = req.queryParams("numeAnimal");
        String idAnimal = req.queryParams("idAnimal");

        try {
            ExamenDTO examen = validateAndBuildExamen(Integer.parseInt(idAnimal), data, anamneza, examenClinic, diagnostic, tratament);
            ExamenDao.insert(examen);
        } catch (Exception e) {
            System.out.println("Error in the examen db");
        }
    }

    private static ExamenDTO validateAndBuildExamen(int petId, String data, String anamneza, String examenClinic, String diagnostic, String tratametn) {
        Date selectedDate = null;
        if (data != null && !data.isEmpty()) {
            try {
                selectedDate = Date.valueOf(data);
            } catch (Exception e) {
                throw new RuntimeException("Invalid due date value: '" + data +
                        "', must be a date in format: dd-mm-yyyy");
            }
        }

        if (anamneza == null || anamneza.isEmpty()) {
            throw new RuntimeException("Anamneza is required!");
        }

        if (examenClinic == null || examenClinic.isEmpty()) {
            throw new RuntimeException("Examen Clinic is required!");
        }

        if (diagnostic == null || diagnostic.isEmpty()) {
            throw new RuntimeException("Diagnostic is required!");
        }

        if (tratametn == null || tratametn.isEmpty()) {
            throw new RuntimeException("Tratament is required!");
        }

        return new ExamenDTO(petId, selectedDate, anamneza, examenClinic, diagnostic, tratametn);
    }
}
